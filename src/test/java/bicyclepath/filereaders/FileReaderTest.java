package bicyclepath.filereaders;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import bicyclepath.graph.GraphComponent;

@RunWith(MockitoJUnitRunner.class)
public class FileReaderTest { 

	private static final String PATH_FAKE = "PATH";
	private static final String THREE_LINES = "\n\n\n";
	
	@Mock GraphComponent component;
	@Mock FileReader<GraphComponent> subject;
	@Mock ComponentParser<GraphComponent> parser;
	
	@Test
	public void readFile(){
		Scanner scanner = new Scanner(THREE_LINES);
		
		when(parser.parse(anyString())).thenReturn(component);
		when(subject.getParser()).thenReturn(parser);
		doCallRealMethod().when(subject).read(anyString());
		doReturn(scanner).when(subject).makeScanner(anyString());
		List<GraphComponent> vertices = subject.read(PATH_FAKE);
		assertThat(vertices, hasSize(3));
	}
	
}