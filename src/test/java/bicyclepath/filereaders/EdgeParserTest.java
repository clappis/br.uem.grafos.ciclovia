package bicyclepath.filereaders;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import bicyclepath.graph.WeightedEdge;
import bicyclepath.graph.Vertice;

@RunWith(MockitoJUnitRunner.class)
public class EdgeParserTest {

	private static final String LINE = "A0001;V0100;V0200;Description";
	
	@Test()
	public void parseLineHappyDay(){
		List<Vertice> vertices = new ArrayList<Vertice>();
		
		Vertice verticeU = newVertice("V0100", 1, 1);
		Vertice verticeW = newVertice("V0200", 4, 5);
		vertices.add(verticeU);
		vertices.add(verticeW);
		addRandomVertices(vertices, 10);
		Collections.shuffle(vertices);
		
		WeightedEdge edge = new EdgePixelParser(vertices).parse(LINE);
		assertThat(edge.getId(), is("A0001"));
		assertThat(edge.getVerticeU(), is(verticeU));
		assertThat(edge.getVerticeW(), is(verticeW));
		assertThat(edge.getWeight(), is(67.5d));
		assertThat(edge.getDescription(), is("Description"));
	}

	private void addRandomVertices(List<Vertice> vertices, int countNewRandomVertices) {
		for (Integer index = 0; index <= countNewRandomVertices; index++) {
			Vertice randomVertice = newVertice(index.toString(), index, index);
			vertices.add(randomVertice);
		}
	}

	private Vertice newVertice(String id, double axisX, double axisY) {
		Vertice randomVertice = new Vertice();
		randomVertice.setId(id.toString());
		randomVertice.setAxisX(axisX);
		randomVertice.setAxisY(axisY);
		return randomVertice;
	}
	
}

