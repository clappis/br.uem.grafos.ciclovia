package bicyclepath.filereaders;

import static bicyclepath.graph.VerticeType.RELEVANT;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import bicyclepath.graph.Vertice;

@RunWith(MockitoJUnitRunner.class)
public class VerticeParserTest {

	private static final String LINE = "V0001;0100;0800;1;Description";
	VerticeParser subject = new VerticeParser();
	
	@Test
	public void parseLineHappyDay(){
		Vertice vertice = subject.parse(LINE);
		assertThat(vertice.getId(), is("V0001"));
		assertThat(vertice.getAxisX(), is(100d));
		assertThat(vertice.getAxisY(), is(800d));
		assertThat(vertice.getType(), is(RELEVANT));
		assertThat(vertice.getDescription(), is("Description"));
	}
	
}

