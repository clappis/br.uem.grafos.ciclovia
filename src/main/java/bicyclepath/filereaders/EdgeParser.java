package bicyclepath.filereaders;

import static java.lang.Double.valueOf;

import java.util.List;

import bicyclepath.graph.Vertice;
import bicyclepath.graph.VerticeFinder;
import bicyclepath.graph.WeightedEdge;

public class EdgeParser implements ComponentParser<WeightedEdge> {

	private VerticeFinder verticeFinder = new VerticeFinder();
	private static final String EDGE_DELIMITADOR = ";";
	private List<Vertice> vertices;

	public EdgeParser(List<Vertice> vertices) {
		this.vertices = vertices;
	}

	public WeightedEdge parse(String line) {
		String[] lineSplit = line.split(EDGE_DELIMITADOR);
		WeightedEdge edge = new WeightedEdge();
		edge.setId(lineSplit[0]);
		edge.setVerticeU(findVerticeById(lineSplit[1]));
		edge.setVerticeW(findVerticeById(lineSplit[2]));
		edge.setWeight(valueOf(lineSplit[3]));
		edge.setDescription((lineSplit[4]));
		return edge;
	}

	private Vertice findVerticeById(String id) {
		return getVerticeFinder().findVerticeById(getVertices(), id);
	}
	
	public VerticeFinder getVerticeFinder() {
		return verticeFinder;
	}

	private List<Vertice> getVertices() {
		return vertices;
	}
}
