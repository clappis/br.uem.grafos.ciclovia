package bicyclepath.filereaders;

import java.util.List;

import org.jgrapht.graph.SimpleWeightedGraph;

import bicyclepath.graph.Vertice;
import bicyclepath.graph.VerticeType;
import bicyclepath.graph.WeightedEdge;

class SimpleWeightedGraphCreator {

	SimpleWeightedGraph<Vertice, WeightedEdge> createGraph(List<Vertice> vertices, List<WeightedEdge> edges, String source){
		SimpleWeightedGraph<Vertice, WeightedEdge> graph = newGraph();
		
		for (Vertice vertice : vertices)
			if (isValidVertice(source, vertice))
				graph.addVertex(vertice);
			
		for (WeightedEdge edge : edges)
			if (isValidVertice(source, edge.getVerticeU()) && isValidVertice(source, edge.getVerticeW()))
				addEdgeToGraph(edge, graph);
		
		return graph;
	}

	private boolean isValidVertice(String source, Vertice vertice) {
		return vertice.getType() != VerticeType.EXTREMITY || vertice.getId().equals(source);
	}

	private void addEdgeToGraph(WeightedEdge edge, SimpleWeightedGraph<Vertice, WeightedEdge> graph) {
		WeightedEdge weightedEdge = graph.addEdge(edge.getVerticeU(), edge.getVerticeW());
		weightedEdge.setId(edge.getId());
		weightedEdge.setWeight(edge.getWeight());
		weightedEdge.setVerticeU(edge.getVerticeU());
		weightedEdge.setVerticeW(edge.getVerticeW());
		weightedEdge.setDescription(edge.getDescription());
		graph.setEdgeWeight(weightedEdge, edge.getWeight());
	}

	private SimpleWeightedGraph<Vertice, WeightedEdge> newGraph() {
		return new SimpleWeightedGraph<Vertice, WeightedEdge>(WeightedEdge.class);
	}
}
