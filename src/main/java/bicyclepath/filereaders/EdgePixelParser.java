package bicyclepath.filereaders;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import java.util.List;

import bicyclepath.graph.Vertice;
import bicyclepath.graph.WeightedEdge;

public class EdgePixelParser implements ComponentParser<WeightedEdge> {

	private static final String EDGE_DELIMITADOR = ";";
	private static final double CONVERSION_FACTOR = 13.5;
	private List<Vertice> vertices;

	public EdgePixelParser(List<Vertice> vertices) {
		this.vertices = vertices;
	}

	public WeightedEdge parse(String line) {
		String[] lineSplit = line.split(EDGE_DELIMITADOR);
		WeightedEdge edge = new WeightedEdge();
		edge.setId(lineSplit[0]);
		Vertice verticeU = findVerticeById(lineSplit[1]);
		Vertice verticeW = findVerticeById(lineSplit[2]);
		edge.setVerticeU(verticeU);
		edge.setVerticeW(verticeW);
		edge.setWeight(calculateWeightByPixels(verticeU, verticeW));
		edge.setDescription((lineSplit[3]));
		return edge;
	}

	private double calculateWeightByPixels(Vertice verticeU, Vertice verticeW) {
		Double x = verticeU.getAxisX() - verticeW.getAxisX();
		Double y = verticeU.getAxisY() - verticeW.getAxisY();
		return sqrt(pow(x, 2) + pow(y, 2)) * CONVERSION_FACTOR;
	}

	private Vertice findVerticeById(String id) {
		return getVertices().stream().filter(x -> x.getId().equals(id)).findAny().get();
	}

	private List<Vertice> getVertices() {
		return vertices;
	}
}
