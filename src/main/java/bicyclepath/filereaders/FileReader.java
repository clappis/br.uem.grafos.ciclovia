package bicyclepath.filereaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import bicyclepath.graph.GraphComponent;

public class FileReader<T extends GraphComponent> {

	private final ComponentParser<T> parser;

	public FileReader(ComponentParser<T> parser) {
		this.parser = parser;
	}

	public List<T> read(String pathFile){
		List<T> components = new ArrayList<T>();
        Scanner scanner = makeScanner(pathFile);
        
		while (scanner.hasNextLine()) 
            components.add(getParser().parse(scanner.nextLine()));
	    
		return components;
	}

	public Scanner makeScanner(String pathFile) {
		try {
			File file = new File(pathFile);
			return new Scanner(file);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public ComponentParser<T> getParser() {
		return parser;
	}
}
