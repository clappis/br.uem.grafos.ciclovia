package bicyclepath.filereaders;

import static java.lang.Double.valueOf;
import bicyclepath.graph.Vertice;
import bicyclepath.graph.VerticeType;

class VerticeParser implements ComponentParser<Vertice>{

	private static final String VERTICE_DELIMITADOR = ";";

	public Vertice parse(String line) {
		String[] lineSplit = line.split(VERTICE_DELIMITADOR);
		Vertice vertice = new Vertice();
		vertice.setId(lineSplit[0]);
		vertice.setAxisX(valueOf(lineSplit[1]));
		vertice.setAxisY(valueOf(lineSplit[2]));
		vertice.setType(VerticeType.value(lineSplit[3]));
		vertice.setDescription(lineSplit[4]);
		return vertice;
	}

	
}
