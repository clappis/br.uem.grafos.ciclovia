package bicyclepath.filereaders;

import bicyclepath.graph.GraphComponent;

public interface ComponentParser<T extends GraphComponent> {

	T parse(String line);
}
