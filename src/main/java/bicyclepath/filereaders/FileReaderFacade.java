package bicyclepath.filereaders;

import java.util.List;

import org.jgrapht.Graph;

import bicyclepath.graph.Vertice;
import bicyclepath.graph.WeightedEdge;

public class FileReaderFacade {
	
	private SimpleWeightedGraphCreator graphCreator = new SimpleWeightedGraphCreator();
	
	public Graph<Vertice, WeightedEdge> read(String pathVertices, String pathEdges, String source){
		List<Vertice> vertices = readVertices(pathVertices);
		List<WeightedEdge> edges = readEdges(vertices, pathEdges);
		return getGraphCreator().createGraph(vertices, edges, source);
	}

	private List<WeightedEdge> readEdges(List<Vertice> vertices, String pathEdges) {
		EdgePixelParser parser = new EdgePixelParser(vertices);
		FileReader<WeightedEdge> reader = new FileReader<WeightedEdge>(parser);
		return reader.read(pathEdges);
	}

	private List<Vertice> readVertices(String pathVertices) {
		VerticeParser parser = new VerticeParser();
		FileReader<Vertice> reader = new FileReader<Vertice>(parser);
		return reader.read(pathVertices);
	}

	protected SimpleWeightedGraphCreator getGraphCreator() {
		return graphCreator;
	}
}
