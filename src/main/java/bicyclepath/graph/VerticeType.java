package bicyclepath.graph;

public enum VerticeType {

	RELEVANT("I"),
	CONNECTION("V"),
	EXTREMITY("E");
	
	private String code;

	VerticeType(String code){
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}

	public static VerticeType value(String code){
		for (VerticeType type : values()) 
			if (type.getCode().equals(code))
				return type;
		
		throw new RuntimeException("Incorrect code type");
	}
}
