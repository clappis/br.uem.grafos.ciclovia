package bicyclepath.graph.algorithm;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import bicyclepath.graph.GraphPathDecorator;
import bicyclepath.graph.Vertice;
import bicyclepath.graph.VerticeFinder;
import bicyclepath.graph.WeightedEdge;

public class DijkstraFacade implements GraphPathAlgorithm {

	private VerticeFinder verticeFinder = new VerticeFinder();
	private String idOrigin;
	private String idDestin;

	public DijkstraFacade(String idOrigin, String idDestin) {
		this.idOrigin = idOrigin;
		this.idDestin = idDestin;
	}
	
	public BicycleGraphPath execute(Graph<Vertice, WeightedEdge> graph) {
		Vertice verticeOrigin = getVerticeFinder().findVerticeById(graph.vertexSet(), getIdOrigin());
		Vertice verticeDestin = getVerticeFinder().findVerticeById(graph.vertexSet(), getIdDestin());
		DijkstraShortestPath<Vertice, WeightedEdge> dijkstra = new DijkstraShortestPath<Vertice, WeightedEdge>(graph);
		GraphPath<Vertice, WeightedEdge> path = dijkstra.getPath(verticeOrigin, verticeDestin);
		return new GraphPathDecorator<Vertice>(path);
	}

	public VerticeFinder getVerticeFinder() {
		return verticeFinder;
	}

	public String getIdOrigin() {
		return idOrigin;
	}

	public String getIdDestin() {
		return idDestin;
	}
}
