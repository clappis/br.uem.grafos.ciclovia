package bicyclepath.graph.algorithm;

import org.jgrapht.Graph;

import bicyclepath.graph.Vertice;
import bicyclepath.graph.WeightedEdge;

public interface GraphPathAlgorithm {

	BicycleGraphPath execute(Graph<Vertice, WeightedEdge> graph);
}
