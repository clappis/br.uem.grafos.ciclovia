package bicyclepath.graph.algorithm;

import java.util.List;

import bicyclepath.graph.WeightedEdge;

public interface BicycleGraphPath {

	List<WeightedEdge> getPath();
	
	double getWeight();
}
