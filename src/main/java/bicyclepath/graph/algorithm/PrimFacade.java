package bicyclepath.graph.algorithm;

import org.jgrapht.Graph;
import org.jgrapht.alg.interfaces.SpanningTreeAlgorithm.SpanningTree;
import org.jgrapht.alg.spanning.PrimMinimumSpanningTree;

import bicyclepath.graph.SpanningTreePath;
import bicyclepath.graph.Vertice;
import bicyclepath.graph.WeightedEdge;

public class PrimFacade implements GraphPathAlgorithm {

	public BicycleGraphPath execute(Graph<Vertice, WeightedEdge> graph) {
		PrimMinimumSpanningTree<Vertice, WeightedEdge> prim = new PrimMinimumSpanningTree<Vertice, WeightedEdge>(graph);
		SpanningTree<WeightedEdge> tree = prim.getSpanningTree();
		return new SpanningTreePath(tree);
	}
	
}
