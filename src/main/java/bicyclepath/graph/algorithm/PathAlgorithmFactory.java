package bicyclepath.graph.algorithm;


public class PathAlgorithmFactory {

	private static final String DIJKSTRA = "2";
	private static final String PRIM = "1";

	private PathAlgorithmFactory(){}
	
	public static GraphPathAlgorithm getInstance(String[] args){
		if (args[0].toUpperCase().equals(PRIM))
			return new PrimFacade();
		if (args[0].toUpperCase().equals(DIJKSTRA))
			return new DijkstraFacade(args[3], args[4]);
		
		throw new RuntimeException("Incorrect code algorithm");
	}
}
