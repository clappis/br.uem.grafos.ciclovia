package bicyclepath.graph;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.alg.interfaces.SpanningTreeAlgorithm.SpanningTree;

import bicyclepath.graph.algorithm.BicycleGraphPath;

public class SpanningTreePath implements BicycleGraphPath {

	private SpanningTree<WeightedEdge> tree;

	public SpanningTreePath(SpanningTree<WeightedEdge> tree){
		this.tree = tree;
	}
	
	@Override
	public List<WeightedEdge> getPath() {
		return new ArrayList<WeightedEdge>(getTree().getEdges());
	}

	@Override
	public double getWeight() {
		return getTree().getWeight();
	}
	
	private SpanningTree<WeightedEdge> getTree() {
		return tree;
	}


}
