package bicyclepath.graph;

import java.util.Collection;
import java.util.Optional;

public class VerticeFinder {

	public Vertice findVerticeById(Collection<Vertice> vertices, String id) {
		Optional<Vertice> vertice = vertices.stream().filter(x -> x.getId().equals(id)).findAny();
		return vertice.isPresent() ? vertice.get() : null;
	}
}
