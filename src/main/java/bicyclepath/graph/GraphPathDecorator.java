package bicyclepath.graph;

import static java.lang.String.format;

import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;

import bicyclepath.graph.algorithm.BicycleGraphPath;

public class GraphPathDecorator<V> implements GraphPath<V, WeightedEdge>, BicycleGraphPath{

	private GraphPath<V, WeightedEdge> pathDecorated;

	public GraphPathDecorator(GraphPath<V, WeightedEdge> path){
		this.pathDecorated = path;
	}

	private GraphPath<V, WeightedEdge> getPathDecorated() {
		return pathDecorated;
	}

	@Override
	public String toString() {
		return format("Path: %s\nWeight: %s",
				getPathDecorated().toString(), 
				getWeight());
	}

	@Override
	public Graph<V, WeightedEdge> getGraph() {
		return getPathDecorated().getGraph();
	}

	@Override
	public V getStartVertex() {
		return getPathDecorated().getStartVertex();
	}

	@Override
	public V getEndVertex() {
		return getPathDecorated().getEndVertex();
	}

	@Override
	public double getWeight() {
		return getPathDecorated().getWeight();
	}

	@Override
	public List<WeightedEdge> getPath() {
		return getPathDecorated().getEdgeList();
	}
	
}
