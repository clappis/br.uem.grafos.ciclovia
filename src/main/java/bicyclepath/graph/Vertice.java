package bicyclepath.graph;

public class Vertice implements GraphComponent {

	private String id;
	private Double axisX;
	private Double axisY;
	private VerticeType type;
	private String description;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getAxisX() {
		return axisX;
	}
	public void setAxisX(Double axisX) {
		this.axisX = axisX;
	}
	public Double getAxisY() {
		return axisY;
	}
	public void setAxisY(Double axisY) {
		this.axisY = axisY;
	}
	public VerticeType getType() {
		return type;
	}
	public void setType(VerticeType type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return getId();
	}
}
