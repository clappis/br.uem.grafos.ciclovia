package bicyclepath.graph;

import org.jgrapht.graph.DefaultWeightedEdge;

public class WeightedEdge extends DefaultWeightedEdge implements GraphComponent {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private Vertice verticeU;
	private Vertice verticeW;
	private double weight;
	private String description;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Vertice getVerticeU() {
		return verticeU;
	}
	public void setVerticeU(Vertice verticeU) {
		this.verticeU = verticeU;
	}
	public Vertice getVerticeW() {
		return verticeW;
	}
	public void setVerticeW(Vertice verticeW) {
		this.verticeW = verticeW;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
