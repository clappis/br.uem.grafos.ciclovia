package bicyclepath;

import static java.lang.System.err;
import static java.lang.System.exit;

import java.io.FileNotFoundException;

import org.jgrapht.Graph;

import bicyclepath.filereaders.FileReaderFacade;
import bicyclepath.graph.Vertice;
import bicyclepath.graph.WeightedEdge;
import bicyclepath.graph.algorithm.BicycleGraphPath;
import bicyclepath.graph.algorithm.GraphPathAlgorithm;
import bicyclepath.graph.algorithm.PathAlgorithmFactory;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		if (args.length < 4){
			err.println("Arguments: <algorhtm> <vertice file path> <edge file path> <source> <sink (only if dijkstra)>");
			exit(1);
		}
		
		Graph<Vertice, WeightedEdge> graph = new FileReaderFacade().read(args[1],args[2], args[3]);
		GraphPathAlgorithm graphAlgorithm = PathAlgorithmFactory.getInstance(args);
		BicycleGraphPath path = graphAlgorithm.execute(graph);
		ByciclePathOutput.execute(path, args[3]);
	}
	
}
