package bicyclepath;

import static java.lang.Double.valueOf;
import static java.lang.String.format;
import static java.lang.System.out;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import bicyclepath.graph.Vertice;
import bicyclepath.graph.VerticeFinder;
import bicyclepath.graph.WeightedEdge;
import bicyclepath.graph.algorithm.BicycleGraphPath;

public class ByciclePathOutput {
	
	public static void main(String[] args) {
		System.out.println(String.format("%04d", 9999));
	}

	public static void execute(BicycleGraphPath path, String resultFilePath) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter("result.txt");
		VerticeFinder verticeFinder = new VerticeFinder();
		
		List<Vertice> vertices = new ArrayList<Vertice>();
		
		for (WeightedEdge edge : path.getPath()) { 
			if (verticeFinder.findVerticeById(vertices, edge.getVerticeU().getId()) == null)
				vertices.add(edge.getVerticeU());
			if (verticeFinder.findVerticeById(vertices, edge.getVerticeW().getId()) == null)
				vertices.add(edge.getVerticeW());
		}
			
		for (Vertice vertice : vertices) 
			writer.print(format("[%s;%04d;%04d;%s]\n",
					vertice.getId(),
					vertice.getAxisX().intValue(), 
					vertice.getAxisY().intValue(),
					vertice.getType().getCode()));
		
		for (WeightedEdge edge : path.getPath()) 
			writer.print(format("[%s;%04d;%04d;%04d;%04d;%04d]\n", 
					edge.getId(), 
					edge.getVerticeU().getAxisX().intValue(),
					edge.getVerticeU().getAxisY().intValue(),
					edge.getVerticeW().getAxisX().intValue(),
					edge.getVerticeW().getAxisY().intValue(),
					valueOf(edge.getWeight()).intValue()));
		
		writer.close();
		out.println("Path: " + path.getPath());
		out.println("Weight: " + path.getWeight());
	}


}
